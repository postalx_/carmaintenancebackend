--------UUID Generate for PSQL---------
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

--------Example CREATE TABLE for PSQL---------

CREATE TABLE users (
    user_id         UUID DEFAULT uuid_generate_v4(),
    name            text,           -- 
    email           text,           -- 
    phone           text,          -- 
    last_update     timestamp
);

CREATE TABLE cars (
    car_id          UUID DEFAULT uuid_generate_v4(),
    make            text,           -- 
    model           text,           -- 
    year            int,          -- 
    odometer        int,           --
    user_id         UUID,
    last_update     date
);


--------Example JOIN Statement---------

SELECT
    users.user_id,
    users.email,
    cars.year,
    cars.odometer
FROM cars
INNER JOIN users ON
     cars.user_id = users.user_id 
WHERE users.user_id  = '6514eb0f-825f-43c2-b49d-df838d4679a9'

ORDER BY cars.year ASC; 