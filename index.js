const express = require('express')
const app = express()
const path = require('path')
const port = 3000
const cors = require('cors')
const bodyParser = require('body-parser')

const testRouter = require('./routes/tests')
const carRouter  = require('./routes/cars')

// app needs to use certain items 
app.use(cors())
app.use('/tests', testRouter);
app.use('/cars', carRouter);

// app.use(bodyParser.urlencoded({ extended: true }));
// app.all('*', (req, res, next) => {
//     res.header("Access-Control-Allow-Origin", "*");
// });
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json())
app.use(
    express.urlencoded({
        extended:true
    })
)





// // yay we made our first endpoint
// app.get('/hello', (request,response) =>{
//     response.json({
//         welcome: "Hello Class of Stellantis-OU Module 3!",
//         date: " Nov 18, 2021",
//         location: "remote"
//     })
//     console.log("our very first console log")
// })

// // lets try adding another endpoint that returns a .html file
// app.get('/html', (request, response) =>{
//     // give me the current path and add the filename to that path to return the file
//     response.sendFile(path.join(__dirname, '/index.html'));
// })

app.listen(port, () => {
    console.log(`Listening on port localhost:${port}.`)
    })
    