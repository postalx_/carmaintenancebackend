const express = require('express');
const db = require('../db');

var carRouter = express.Router();

carRouter.use(express.json())

// show proof of concept for limited endpoints
// in postman, go to headers tab - create new header
// for key, type "admin" - for value type "true"
carRouter.get('/all', (request,response) => {
    console.log('admin header', request.headers.admin)
    if (request.headers.admin){
        console.log("admin detected")
        databaseCall('SELECT * FROM cars;',request,response);
    }
    else {
        response.status(403).set('Access-Control-Allow-Origin', '*').json("Not Authorized")
    }
})

//use parameters. NOTE that SQL statement uses `` instead of '' since the '' is inside 
// use car_id for one example from previous /all endpoint
// if you are struggling with the header, you can move the databaseCall line outside of the if statement and delete the if statment
//
// to hit this endpoint, use localhost:3000/cars/def10125-8aae-48e3-ae63-58c6edfed4e1
// the UUID (def10125-8aae-48e3-ae63-58c6edfed4e1) will be different for your database
carRouter.get('/:cardId', (request,response) => {

    databaseCall(`SELECT * FROM cars WHERE car_id = '${request.params.cardId}';`,request,response);
})


// notice RETURNING * at the end of SQL to give me the object that was just created

// This body needs to be sent via postman > body > raw JSON 
// example body: 

// {
//     "year": "2017",
//     "odometer": 123123
// } 
carRouter.post('/create', (request,response) =>{
    console.log('request.body',request.body);
    databaseCall(`INSERT INTO cars (make, model,year, odometer) VALUES ('${request.body.make}','${request.body.model}','${request.body.year}','${request.body.odometer}')
    RETURNING *; `, request,response);
    
})


// here we will use the body to pass the car_id and updated parameters
// expects year, odometer, and car_id in body
// example body: 

// {
//     "carId":"def10125-8aae-48e3-ae63-58c6edfed4e1",
//     "year": "2017",
//     "odometer": 123123
// } 

// these can be hard to debug. better try your SQL statements directly in DB
// by logging into your postgres using "psql ..." from command line
carRouter.put('/update/', (request,response) => {
    console.log('request.body',request.body);
    databaseCall(`UPDATE cars SET year = '${request.body.year}',
         odometer = '${request.body.odometer}'
         WHERE car_id = '${request.body.carId}' RETURNING * ;  `
         ,request,response);
   
 
})


// notice this is the same route as get for a specifc car
// if delete successful, a [] will be shown in the postman body
carRouter.delete('/:cardId', (request,response) => {
    databaseCall(`DELETE FROM cars WHERE car_id = '${request.params.cardId}';`,request,response);
})


// -------------- IF YOUVE MADE IT THIS FAR ----------------
// ------------- AND ARE LOOKING FOR MORE FUN --------------
// 1. use this template to make a users/ route where you can CRUD
// create, read, update, delete users
// 2. create cars with userids
// 3. join car and user info using a SQL join statement 




const databaseCall = (query, request,response) =>{
    db.query(query, (error, results) => {
        if (error) {
            // throw error
            console.log(error)
            response.status(500).json({"error":error, "message": "Most likely a SQL statement error. Make sure your SQL statement worksf first  by logging into your psql database and checking there."});
        }
        else {
            // response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            // response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, admin');
            response.set('Access-Control-Allow-Origin', 'http://localhost:19006');
            response.status(200).json(results.rows);
        }   
    })
}


module.exports = carRouter;