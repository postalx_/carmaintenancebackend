const express = require('express');
const db = require('../db');

var testRouter = express.Router();

testRouter.use(express.json())

testRouter.post('/sql', (request,response) => {
    console.log('request.body',request.body);
    response.status(200).json("success");
 
})

testRouter.get('/sql', (request,response) => {

    console.log('before async', Date.now())
    getUsers('SELECT * FROM users;',request,response);
    console.log('after async', Date.now())
})

const getUsers = (query, request,response) =>{
    
    db.query(query, (error, results) => {
        if (error) {
            // throw error
            response.status(500).json(error);
        }
        console.log('inside async', Date.now())
        response.status(200).json(results.rows)
    })
    console.log('inside getusers', Date.now())
}

// yay we made our first endpoint
testRouter.get('/hello', (request,response) =>{
    response.json({
        welcome: "Hello Class of Stellantis-OU Module 3!",
        date: " Nov 18, 2021",
        location: "remote"
    })
    console.log("our very first console log!!!!")
})

// lets try adding another endpoint that returns a .html file
testRouter.get('/html', (request, response) =>{
    // give me the current path and add the filename to that path to return the file
    response.sendFile(path.join(__dirname, '/index.html'));
})

module.exports = testRouter;